package com.example.magic.ui.main;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface CardDao {
    @Query("select * from card")
    LiveData<List<Card>> getCards();

    @Insert
    void addCard(Card movie);

    @Insert
    void addCards(List<Card> movies);

    @Delete
    void deleteCard(Card movie);

    @Query("DELETE FROM card")
    void deleteCards();
}
