package com.example.magic.ui.main.ui.main;

import androidx.lifecycle.ViewModelProvider;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.magic.R;
import com.example.magic.databinding.CartadetalleBinding;
import com.example.magic.databinding.MainFragmentBinding;
import com.example.magic.ui.main.Card;

public class DetalleCartaFragment extends Fragment {

    private View view;
    private TextView nombre;
    private TextView texto;
    private TextView rareza;
    private TextView tipo;
    private TextView color;
    private ImageView imagen;

    private MainViewModel2 mViewModel;

    private @NonNull CartadetalleBinding binding;

    public static DetalleCartaFragment newInstance() {
        return new DetalleCartaFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = CartadetalleBinding.inflate(inflater);

        view = binding.getRoot();

        Intent i = getActivity().getIntent();

        if (i != null) {
            Card carta = (Card) i.getSerializableExtra("Card");

            if (carta != null) {
                updateUi(carta);
            }
        }

        return view;
    }

    @SuppressLint("SetTextI18n")
    private void updateUi(Card carta) {
        binding.textoCarta.setText(carta.getText()+"\n"+carta.getFlavor());
        binding.nombreCarta.setText(carta.getName());
        binding.rarezaCarta.setText(carta.getRarity());
        binding.colorCarta.setText(carta.getColors());
        binding.tipoCarta.setText(carta.getType());


        Glide.with(getContext()).load(
                carta.getImagen()
        ).apply(new RequestOptions().override(1000,500)).into(binding.imagenCarta);


    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = new ViewModelProvider(this).get(MainViewModel2.class);
        // TODO: Use the ViewModel
    }



}