package com.example.magic.ui.main;

import android.net.Uri;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

public class CardsApi {
    //     private final String base_url ="https://api.magicthegathering.io/v1";
    ArrayList<Card> getCartas(){
        String base_url = "https://api.magicthegathering.io/v1";
        Uri bulrUri = Uri.parse(base_url)
                .buildUpon()
                .appendPath("cards")
                .build();
        String url = bulrUri.toString();
        return doCall(url);
    }

    private ArrayList<Card>  doCall(String url) {
        try {
            String JsonResponse = HttpUtils.get(url);
            return processJson(JsonResponse);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private ArrayList<Card> processJson(String jsonResponse) {
        ArrayList<Card> Cartas = new ArrayList<>();
        try {
            JSONObject data = new JSONObject(jsonResponse);
            JSONArray jsonCartas = data.getJSONArray("cards");
            for (int i = 0; i <jsonCartas.length() ; i++) {
                JSONObject jsoncarta = jsonCartas.getJSONObject(i);

                Card carta = new Card();
                try {
                    carta.setName(jsoncarta.getString("name"));
                    carta.setType(jsoncarta.getString("type"));
                    carta.setText(jsoncarta.getString("text"));
                    carta.setImagen(jsoncarta.getString("imageUrl"));
                    carta.setRarity(jsoncarta.getString("rarity"));
                    try {
                        carta.setColors(jsoncarta.getJSONArray("colors").getString(0));
                    }catch (Exception e){
                        carta.setColors(jsoncarta.getString("colors"));
                    }
                    try{
                        carta.setFlavor(jsoncarta.getString("flavor"));
                    }catch (Exception e){
                        carta.setFlavor("");
                    }
                    Cartas.add(carta);
                }catch (Exception e){
                    System.out.println("no se ha añadido");
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return Cartas;
    }
}
