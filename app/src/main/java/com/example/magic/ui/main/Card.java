package com.example.magic.ui.main;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import org.jetbrains.annotations.NotNull;

import java.io.Serializable;

@Entity
public class Card implements Serializable {
    @PrimaryKey(autoGenerate = true)
    private int id;
    private String name;
    private String colors;
    private String type;
    private String text;
    private String imagen;
    private String rarity;
    private String flavor;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFlavor() {
        return flavor;
    }

    public void setFlavor(String flavor) {
        this.flavor = flavor;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColors() {
        return colors;
    }

    public void setColors(String colors) {
        this.colors = colors;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public String getRarity() {
        return rarity;
    }

    public void setRarity(String rarity) {
        this.rarity = rarity;
    }

    @NotNull
    @Override
    public String toString() {
        return "Card{" +
                "name='" + name + '\'' +
                ", colors='" + colors + '\'' +
                ", type='" + type + '\'' +
                ", text='" + text + '\'' +
                ", imagen='" + imagen + '\'' +
                ", rarity='" + rarity + '\'' +
                ", flavor='" + flavor + '\'' +
                '}';
    }
}
