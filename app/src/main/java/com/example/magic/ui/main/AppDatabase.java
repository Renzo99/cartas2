package com.example.magic.ui.main;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;


@Database(entities = {Card.class},version = 1)
public abstract class AppDatabase  extends RoomDatabase {
    private static AppDatabase Instance;

    public static AppDatabase getDatabase(Context context){
        if(Instance == null){
            Instance = Room.databaseBuilder(
                    context.getApplicationContext(),
                    AppDatabase.class,"db"
            ).build();
        }
        return Instance;
    }

    public abstract CardDao getCards();
}
