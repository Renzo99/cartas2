package com.example.magic.ui.main;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;


import com.example.magic.R;
import com.example.magic.databinding.MainFragmentBinding;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class MainFragment extends Fragment {

    private MainViewModel model;
    private ArrayList<Card> items;
    private CartasAdapter adapter;
    private MainFragmentBinding binding;
    private ProgressDialog dialog;

    public static MainFragment newInstance() {
        return new MainFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = MainFragmentBinding.inflate(inflater);
        View view = binding.getRoot();

        items = new ArrayList<>();

        adapter = new CartasAdapter (
                getContext(),
                R.layout.adapter,
                items
        );
        binding.lvcartas.setAdapter(adapter);

        binding.lvcartas.setOnItemClickListener((adapter, fragment, i, l) -> {
            Card card  = (Card) adapter.getItemAtPosition(i);
            Intent intent = new Intent(getContext(), DetalleCarta.class);
            intent.putExtra("Card", card);

            startActivity(intent);
        });

        model = ViewModelProviders.of(this).get(MainViewModel.class);
        model.getCards().observe(this, cards ->{
          adapter.clear();
          adapter.addAll(cards);
        });

        refresh();
        dialog = new ProgressDialog(getContext());
        dialog.setMessage("Loading...");

        model.getLoading().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean mostrat) {
                if (mostrat)
                    dialog.show();
                else
                    dialog.dismiss();
            }
        });


        return view;

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // TODO: Use the ViewModel
    }

    public void onCreateOptionsMenu(Menu menu, @NotNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_cartas, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_refresh) {
            refresh();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void refresh() {
        model.reload();
    }

    private class RefreshDataTask extends AsyncTask<Void, Void, ArrayList<Card>> {
        @Override
        protected ArrayList<Card> doInBackground(Void... voids) {
            CardsApi api = new CardsApi();
            //ArrayList<Card> result = api.getCartas();
            //Log.d("DEBUG", result);

            return api.getCartas();
        }

        @Override
        protected void onPostExecute(ArrayList<Card> cards) {
            adapter.clear();
            for (Card carta : cards) {
                adapter.add(carta);
            }
        }
    }

}