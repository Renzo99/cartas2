package com.example.magic.ui.main;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.magic.R;
import com.example.magic.ui.main.ui.main.DetalleCartaFragment;

public class DetalleCarta extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detalle_carta_activity);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, DetalleCartaFragment.newInstance())
                    .commitNow();
        }
    }
}