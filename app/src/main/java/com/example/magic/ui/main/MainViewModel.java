package com.example.magic.ui.main;

import android.app.Application;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;
import java.util.List;

public class MainViewModel extends AndroidViewModel {
    private final Application app;
    private final CardDao cardDao;
    private LiveData<List<Card>> cards;
    private MutableLiveData<Boolean> loading;


    public MainViewModel(Application application) {
        super(application);

        this.app = application;
        AppDatabase appDatabase = AppDatabase.getDatabase(
                this.getApplication());
        this.cardDao = appDatabase.getCards();
    }
    public LiveData<List<Card>> getCards(){
        return cardDao.getCards();
    }
    public void reload(){
        RefreshDataTask task = new RefreshDataTask();
        task.execute();
    }

    public MutableLiveData<Boolean> getLoading(){
        if(loading == null){
            loading = new MutableLiveData<>();
        }
        return loading;
    }

    private class RefreshDataTask extends AsyncTask<Void, Void, ArrayList<Card>> {
        @Override
        protected ArrayList<Card> doInBackground(Void... voids) {

            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(
                    app.getApplicationContext()
            );

            CardsApi api = new CardsApi();
            ArrayList<Card> result;
            result = api.getCartas();
            cardDao.deleteCards();
            cardDao.addCards(result);
            return null;
        }
        @Override
        protected void onPostExecute(ArrayList<Card> cards) {
            super.onPostExecute(cards);
            loading.setValue(false);
        }
    }
}