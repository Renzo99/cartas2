package com.example.magic.ui.main;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.databinding.DataBindingUtil;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.magic.R;
import com.example.magic.databinding.AdapterBinding;

import java.util.List;

public class CartasAdapter extends ArrayAdapter<Card> {
    public CartasAdapter(Context context, int resource, List<Card> objects){
        super(context,resource,objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){

        AdapterBinding binding;

        Card carta = getItem(position);

        if (convertView  == null){
            LayoutInflater inflater = LayoutInflater.from(getContext());
            binding = DataBindingUtil.inflate(inflater, R.layout.adapter, parent, false);
        }else {
            binding = DataBindingUtil.getBinding(convertView);
        }

        assert binding != null;
        binding.nombre.setText(carta.getName());
            binding.tipo.setText(carta.getType());
            binding.rarity.setText(carta.getRarity());
            // texto.setText(carta.getText());
            // System.out.println("- - - - - - " + carta.getImagen());

            Glide.with(getContext()).load(
                    carta.getImagen()
            ).apply(new RequestOptions().override(800,200)).into(binding.carta);

        return  binding.getRoot();
    }


}
